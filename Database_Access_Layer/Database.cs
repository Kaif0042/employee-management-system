﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using EmployeeManagementSystem.Models;



namespace EmployeeManagementSystem.Database_Access_Layer
{
    public class Database
    {
        SqlConnection MyCon = new SqlConnection(ConfigurationManager.ConnectionStrings["MyCon"].ConnectionString);

        public void Add_Record(Employee emp)
        {
            SqlCommand com = new SqlCommand("sp_Employee_Add",MyCon);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Name",emp.Name);
            com.Parameters.AddWithValue("@Date_Of_Joining",emp.Date_Of_Joining);
            com.Parameters.AddWithValue("@Department",emp.Department);
            com.Parameters.AddWithValue("@Description",emp.Description);
            MyCon.Open();
            com.ExecuteNonQuery();
            MyCon.Close();

        }

        public void Update_Record(Employee emp)
        {
            SqlCommand com = new SqlCommand("sp_Employee_Update", MyCon);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", emp.Id);
            com.Parameters.AddWithValue("@Name", emp.Name);
            com.Parameters.AddWithValue("@Date_Of_Joining", emp.Date_Of_Joining);
            com.Parameters.AddWithValue("@Department", emp.Department);
            com.Parameters.AddWithValue("@Description", emp.Description);
            MyCon.Open();
            com.ExecuteNonQuery();
            MyCon.Close();
        }

        public DataSet Record_By_Id(int Id)
        {
            SqlCommand com = new SqlCommand("sp_Employee_Search", MyCon);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id",Id);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public DataSet Show_Data()
        {
            SqlCommand com = new SqlCommand("sp_Employee_Show_All", MyCon);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public DataSet Show_By_Dept(string Department)
        {
            SqlCommand com = new SqlCommand("sp_Show_By_Dept", MyCon);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Department",Department);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public void Delete_Record(int Id)
        {
            SqlCommand com = new SqlCommand("sp_Employee_Delete", MyCon);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", Id);
            MyCon.Open();
            com.ExecuteNonQuery();
            MyCon.Close();
        }

        public DataSet Department_By_Id(int Id)
        {
            SqlCommand com = new SqlCommand("sp_Department_Search", MyCon);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", Id);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public DataSet Show_Dept()
        {
            SqlCommand com = new SqlCommand("sp_Show_Dept", MyCon);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }


        public void Add_Department(Department dept)
        {
            SqlCommand com = new SqlCommand("sp_Department_Add", MyCon);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Name", dept.Name);
            com.Parameters.AddWithValue("@Description", dept.Description);
            MyCon.Open();
            com.ExecuteNonQuery();
            MyCon.Close();

        }

        public void Update_Department(Department dept)
        {
            SqlCommand com = new SqlCommand("sp_Department_Update", MyCon);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", dept.Id);
            com.Parameters.AddWithValue("@Name", dept.Name);
            com.Parameters.AddWithValue("@Description", dept.Description);
            MyCon.Open();
            com.ExecuteNonQuery();
            MyCon.Close();
        }

        public void Delete_Department(int Id)
        {
            SqlCommand com = new SqlCommand("sp_Department_Delete", MyCon);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", Id);
            MyCon.Open();
            com.ExecuteNonQuery();
            MyCon.Close();
        }


    }
}