﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeManagementSystem.Models
{
    public class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Date_Of_Joining { get; set; }

        public string Department { get; set; }

        public string Description { get; set; }
        
        public List<SelectListDepartment> list { get; set; }
        public class SelectListDepartment
        {
            public string Name { get; set; }
            public int Id { get; set; }
        }
    }
}