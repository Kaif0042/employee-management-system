﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using EmployeeManagementSystem.Models;
using System.Security.Cryptography;
using System.Configuration;
using System.Data.SqlClient;
using static EmployeeManagementSystem.Models.Employee;

namespace EmployeeManagementSystem.Controllers
{
    public class HomeController : Controller
    {

        Database_Access_Layer.Database Database_Layer = new Database_Access_Layer.Database();

        public ActionResult Index()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Show_Data()
        {
            DataSet ds = Database_Layer.Show_Data();
            //ViewBag.emp = ds.Tables[0];
            DataTable dt= ds.Tables[0];
            return View(dt);
        }

        public ActionResult Add_Record()
        {
            var GetList = GetDepartmentlist().ToList();
            ViewBag.Department = (from department in GetDepartmentlist() select new SelectListDepartment { Id = department.Id, Name = department.Name }).ToList();
            return View();
        }

        public List<SelectListDepartment> GetDepartmentlist()
        {
            List<SelectListDepartment> list = new List<SelectListDepartment>();
            string CS = ConfigurationManager.ConnectionStrings["MyCon"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);
            {
                string query = "select Id,Name from Department";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        list.Add(new SelectListDepartment
                        {
                            Id = Convert.ToInt32(rdr["Id"]),
                            Name = rdr["Name"].ToString()
                        });
                    }

                }
                con.Close();
            }
            return list;
        }


        [HttpPost]

        public ActionResult Add_Record(FormCollection fc)
        {
            Employee emp = new Employee();
            emp.Name = fc["Name"];
            emp.Date_Of_Joining = fc["Date_Of_Joining"];
            emp.Department = fc["Department"];
            emp.Description = fc["Description"];
            Database_Layer.Add_Record(emp);
            TempData["msg"] = "Data Inserted";
            ViewBag.Department = (from department in GetDepartmentlist() select new SelectListDepartment { Id = department.Id, Name = department.Name }).ToList();
            return RedirectToAction("Show_Data");
            //return View();
        }

        public ActionResult Update_Record(int Id)
        {
            DataSet ds = Database_Layer.Record_By_Id(Id);
           // ViewBag.emprecord=ds.Tables[0];
            DataTable dt = ds.Tables[0];
            ViewBag.Department = (from department in GetDepartmentlist() select new SelectListDepartment { Id = department.Id, Name = department.Name }).ToList();
            //ViewBag.Department = new SelectList(GetDepartmentlist(), "Id", "Name", emp.Department.ToString());
            return View(dt);
        }

        [HttpPost]
        public ActionResult Update_Record(int Id,FormCollection fc)
        {
            Employee emp = new Employee();
            emp.Name = fc["Name"];
            emp.Date_Of_Joining = fc["Date_Of_Joining"];
            emp.Department = fc["Department"];
            emp.Description = fc["Description"];emp.Id = Id;
            Database_Layer.Update_Record(emp);
            TempData["msg"] = "Data Updated";
            return RedirectToAction("Show_Data");
        }

        public ActionResult Delete_Record(int Id)
        {
            Database_Layer.Delete_Record(Id);
            TempData["msg"] = " Record Deleted";

            return RedirectToAction("Show_Data");
        }

        public ActionResult Record_By_Id()
        {
            DataSet ds = Database_Layer.Show_Data();
            
            DataTable dt = ds.Tables[0];
            return View(dt);
        }

        public ActionResult Show_By_Dept()
        {
            DataSet ds = Database_Layer.Show_Data();
            
            DataTable dt = ds.Tables[0];
            return View(dt);
        }


        public ActionResult Show_Dept()
        {
            DataSet ds = Database_Layer.Show_Dept();
            DataTable dt = ds.Tables[0];
            return View(dt);
        }

        public ActionResult Add_Department()
        {
          return View();
        }

        [HttpPost]
        public ActionResult Add_Department( FormCollection fa)
        {
            Department dept = new Department();
            dept.Name = fa["Name"];
            dept.Description = fa["Description"];
            Database_Layer.Add_Department(dept);
            TempData["msg"] = "Data Inserted";
            return RedirectToAction("Show_Dept");
        }


        public ActionResult Update_Department(int Id)
        {
            DataSet ds = Database_Layer.Department_By_Id(Id);
            DataTable dt = ds.Tables[0];
            return View(dt);
        }

        [HttpPost]
        public ActionResult Update_Department(int Id, FormCollection fa)
        {
            Department dept = new Department();
            dept.Name = fa["Name"];
            dept.Description = fa["Description"]; dept.Id = Id;
            Database_Layer.Update_Department(dept);
            TempData["msg"] = "Data Updated";
            return RedirectToAction("Show_Dept");
        }

        public ActionResult Delete_Department(int Id)
        {
            Database_Layer.Delete_Department(Id);
            return RedirectToAction("Show_Dept");
        }

        
    }
}